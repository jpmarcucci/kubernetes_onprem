# Laboratorio Kubernetes

## Infraestrutura

Basico para um servidor funcional com CoreDNS, Calico e RBAC  
`kubectl apply -f infra/rbac/`  
`kubectl apply -f infra/calico-etcd.yaml`  
`kubectl apply -f infra/coredns.yaml`  

Servidor de metricas (opcional)  
`kubectl apply -f infra/metrics-server/`  

Metallb (opcional)  
`kubectl apply -f infra/metallb.yaml`  
`kubectl apply -f infra/metallb-configuration.yaml`  

Traefik (Opcional)  
`helm install ./traefik-helm-chart/traefik/ --generate-name`  

## Exemplos

Deployment Simples Nginx + Service  
`kubectl apply -f exemplos/simple-nginx-deployment/nginx-deployment.yaml`  

Deployment Apache + Service com HPA (Horizontal Pod Autoscaler)  
`kubectl apply -f exemplos/apache-hpa/apache-hpa.yaml`  

Deployment Nginx com Traefik  
`kubectl apply -f exemplos/nginx-traefik/`  

Wordpress + MySQL + PV + PVC  
`kubectl apply -f exemplos/wordpress/`  
